//
//  ListView.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import FirebaseDatabase
import UIKit

class ListView: UIViewController {
    
    
    @IBOutlet weak var tableMovie: UITableView!
    
    
    var questions = [Questions]()
    var ref: DatabaseReference!
    var presenter: ListPresenterProtocol?
    var colorBackground : UIColor = .white


    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        self.getRemoteColor()
        self.colorObserver()
        self.prepareUI()
        self.presenter?.viewDidLoad()
    }
}

extension ListView: ListViewProtocol {
    func showGraphics(category: servicio) {
        guard let array = category.questions else {return}
        self.questions.removeAll()
        self.questions = array
        self.tableMovie.reloadData()
    }
}

extension ListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GraphicCell") as!  GraphicCell
        cell.question = questions[indexPath.row]
        cell.colorBackground = self.colorBackground
        return cell
    }
}

extension ListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350.0
    }
}

extension ListView{
    func prepareUI(){
        let nameCell = "GraphicCell"
        let nib = UINib(nibName: nameCell, bundle: nil)
        self.tableMovie.register(nib, forCellReuseIdentifier: nameCell)
    }
    
    
    
    private func colorObserver(){
        ref.child("background_screen_two").observe(.childChanged){ (snapshot) in
            if let value = snapshot.value as? String{
                self.setColor(color : value)
            }
        }
    }
    
    private func getRemoteColor(){
        ref.child("background_screen_two/color").getData(completion:  { error, snapshot in
          guard error == nil else {
            print(error!.localizedDescription)
            return;
          }
          if let value = snapshot.value as? String{
              self.setColor(color : value)
          }
        })
    }
    
    private func setColor(color : String){
        self.colorBackground = UIColor(hexaString: color)
        self.view.backgroundColor = self.colorBackground
        self.tableMovie.backgroundColor = self.colorBackground
        self.tableMovie.reloadData()
    }
    
}
