//
//  ListProtocols.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import UIKit

protocol ListViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    var presenter: ListPresenterProtocol? { get set }
    func showGraphics(category: servicio)
    
}

protocol ListWireFrameProtocol: AnyObject {
    // PRESENTER -> WIREFRAME
    static func createListModule(category: servicio) -> UIViewController
}

protocol ListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var view: ListViewProtocol? { get set }
    var interactor: ListInteractorInputProtocol? { get set }
    var wireFrame: ListWireFrameProtocol? { get set }
    var objectsGraphics: servicio? { get set }
    func viewDidLoad()
}

protocol ListInteractorOutputProtocol: AnyObject {
// INTERACTOR -> PRESENTER
}

protocol ListInteractorInputProtocol: AnyObject {
    // PRESENTER -> INTERACTOR
    var presenter: ListInteractorOutputProtocol? { get set }
    var localDatamanager: ListLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: ListRemoteDataManagerInputProtocol? { get set }
}

protocol ListDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> DATAMANAGER
}

protocol ListRemoteDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol? { get set }
}

protocol ListRemoteDataManagerOutputProtocol: AnyObject {
    // REMOTEDATAMANAGER -> INTERACTOR
}

protocol ListLocalDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> LOCALDATAMANAGER
}
