//
//  ListPresenter.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation

class ListPresenter  {
    
    // MARK: Properties
    weak var view: ListViewProtocol?
    var interactor: ListInteractorInputProtocol?
    var wireFrame: ListWireFrameProtocol?
    var objectsGraphics: servicio?
}

extension ListPresenter: ListPresenterProtocol {
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        guard let objGraphics = self.objectsGraphics else {return}
        view?.showGraphics(category: objGraphics)
        
    }
}

extension ListPresenter: ListInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
