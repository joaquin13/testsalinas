//
//  HomeInteractor.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation

class HomeInteractor: HomeInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: HomeInteractorOutputProtocol?
    var localDatamanager: HomeLocalDataManagerInputProtocol?
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol?
    
    func getExternalData() {
        remoteDatamanager?.externalGetData()
    }

}

extension HomeInteractor: HomeRemoteDataManagerOutputProtocol {
    func callBackData(objGraphics: servicio) {
        presenter?.callBackData(objGraphics: objGraphics)
    }
    
}
