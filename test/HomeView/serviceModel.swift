//
//  serviceModel.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation

struct servicio: Codable {
    
    var colors:[String]?
    var questions:[Questions]?
    
    enum CodingKeys: String, CodingKey {
        case colors = "colors"
        case questions = "questions"
    }
}


struct Questions: Codable {
    var total: Int?
    var text: String = ""
    var chartData:[CharData]?
    
    enum CodingKeys: String, CodingKey {
        case total = "total"
        case text = "text"
        case chartData = "chartData"
    }
}

struct CharData: Codable {
    var percetnage: Int?
    var text: String = ""
    
    enum CodingKeys: String, CodingKey {
        case percetnage = "percetnage"
        case text = "text"
    }
}
