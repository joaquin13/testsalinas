//
//  HomeRemoteDataManager.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import Alamofire

class HomeRemoteDataManager:HomeRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    
    func externalGetData() {
        AF.request("https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld", method: .get).responseDecodable(of: servicio.self) { response in
            guard let data = response.value else { return }
            self.remoteRequestHandler?.callBackData(objGraphics: data)
        }
    }
}
