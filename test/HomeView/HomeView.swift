//
//  HomeView.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import UIKit

class HomeView: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btn: UIButton!
    
    
    
    
    var stringTitle = [String]()
    let imagePicker  = UIImagePickerController()
    var imagen : UIImage!
    var objGraphics : servicio?
    var colorBackground : UIColor = .white
    var textFieldName: UITextField?
    
    var ref: DatabaseReference!
    let storage = Storage.storage().reference()
    
    

    
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stringTitle.append("Nombre")
        self.stringTitle.append("Camara")
        self.stringTitle.append("Graficas")
        self.ref = Database.database().reference()
        self.getRemoteColor()
        self.colorObserver()
        self.prepareUI()
        self.presenter?.viewDidLoad()
    }
    
    func prepareUI(){
        self.btn.isEnabled = false
        let nameCell = "NameCell"
        let nib = UINib(nibName: nameCell, bundle: nil)
        self.table.register(nib, forCellReuseIdentifier: nameCell)
    }
    
    private func colorObserver(){
        ref.child("background_screen_one").observe(.childChanged){ (snapshot) in
            if let value = snapshot.value as? String{
                self.setColor(color : value)
            }
        }
    }
    
    private func getRemoteColor(){
        ref.child("background_screen_one/color").getData(completion:  { error, snapshot in
          guard error == nil else {
            print(error!.localizedDescription)
            return;
          }
          if let value = snapshot.value as? String{
              self.setColor(color : value)
          }
        })
    }
    
    private func setColor(color : String){
        self.colorBackground = UIColor(hexaString: color)
        self.view.backgroundColor = self.colorBackground
        self.table.backgroundColor = self.colorBackground
        self.table.reloadData()
    }
    
    @IBAction func actionUploadFile(_ sender: UIButton) {
        
        guard let imageData = self.imagen.pngData() else {return}
        
        let alertMain = UIAlertController(title: "Subiendo Archivo", message: nil, preferredStyle: UIAlertController.Style.alert)
        self.present(alertMain, animated: true, completion: nil)
        
        let name = self.textFieldName?.text ?? ""
        
        self.storage.child("images/\(name).png").putData(imageData, metadata: nil, completion: { _, error in
            guard error == nil else {return}
            alertMain.dismiss(animated: true, completion: nil)
            let alert = UIAlertController(title: "Archivo subido con exito", message: nil, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
            self.present(alert, animated: true, completion: nil)
        })
        
    }

    
}

extension HomeView: HomeViewProtocol {
    // TODO: implement view output methods
    func callBackData(objGraphics : servicio) {
        self.objGraphics = objGraphics
    }
    
    func iniciar() {
        DispatchQueue.main.async {
            self.loadIndicator.startAnimating()
        }
    }
    
    func detener() {
        DispatchQueue.main.async {
            self.loadIndicator.stopAnimating()
            self.loadIndicator.hidesWhenStopped = true
        }
    }
    
    
}

extension HomeView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var cell : UITableViewCell?
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as! NameCell
            cell.colorBackground = self.colorBackground
            self.textFieldName = cell.textFieldName
            return cell
        default:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "celda")
            cell.contentView.backgroundColor = self.colorBackground
            cell.textLabel?.text = stringTitle[indexPath.row]
            cell.textLabel?.textAlignment = .center
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }

}

extension HomeView: UITableViewDelegate {
    // TODO: implement view output methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 1){
            self.getSetImage()
        }else if(indexPath.row == 2){
            guard let obj = self.objGraphics else {return}
            self.presenter?.getDataNextView(objectList: obj)
        }
    }
}

extension HomeView{
    func getSetImage() {
        func tomarFoto(alert: UIAlertAction!) {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    imagePicker.allowsEditing = true
                    imagePicker.sourceType = .camera
                    imagePicker.cameraCaptureMode = .photo
                    imagePicker.delegate = self
                    present(imagePicker, animated: true, completion: nil)
                }
            }
        }
        
        func mostrarFoto(alert: UIAlertAction!) {
            if(imagen != nil){
                let showAlert = UIAlertController(title: "Imagen", message: nil, preferredStyle: .alert)
                        
                let imageView = UIImageView(frame: CGRect(x: 60, y: 60, width: 150, height: 230))
                imageView.image = self.imagen
                showAlert.view.addSubview(imageView)
                let height = NSLayoutConstraint(item: showAlert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 360)
                let width = NSLayoutConstraint(item: showAlert.view!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                showAlert.view.addConstraint(height)
                showAlert.view.addConstraint(width)
                showAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(showAlert, animated: true, completion: nil)
            }
        }
        
        let cadena : String = "Selecione una accion"
        let alert = UIAlertController(title: "Camara", message: cadena, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Tomar Foto", style: UIAlertAction.Style.default, handler: tomarFoto))
        alert.addAction(UIAlertAction(title: "Cargar Foto", style: UIAlertAction.Style.default, handler: mostrarFoto))
        self.present(alert, animated: true, completion: nil)
        
        
    }
}


extension HomeView : UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imagenselect : UIImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.imagen = imagenselect
            self.btn.isEnabled = true
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
}
