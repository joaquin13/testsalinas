//
//  HomePresenter.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation

class HomePresenter  {
    
    var stringCollection = [String]()
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
}

extension HomePresenter: HomePresenterProtocol {
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        //Se consulta al interactor para extraer los datos remotos
        interactor?.getExternalData()
        view?.iniciar()
    }
    
    func getDataNextView(objectList: servicio){
        wireFrame?.presentNextView(from: view!, objectList: objectList)
        
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    // TODO: implement interactor output methods
    func callBackData(objGraphics : servicio) {
        view?.callBackData(objGraphics : objGraphics)
        view?.detener()
    }
}
