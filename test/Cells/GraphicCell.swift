//
//  GraphicCell.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import UIKit
import Charts

class GraphicCell : UITableViewCell, ChartViewDelegate{
    
    @IBOutlet weak var lblNameQuestion: UILabel!
    @IBOutlet weak var viewBody: UIView!
    
    var pieChart = PieChartView()
    var question : Questions?
    
    public var colorBackground : UIColor = .white

    override func awakeFromNib() {
        super.awakeFromNib()
        self.pieChart.delegate = self
    }
    
    override func layoutSubviews() {
        guard let objQuestion = self.question, let answersArray = objQuestion.chartData else {return}
        self.lblNameQuestion.text = objQuestion.text
        self.pieChart.frame = CGRect(x : 0 ,
                                y: 0,
                                width: 280,
                                height: 280)
        self.pieChart.center = self.contentView.center
        self.viewBody.addSubview(self.pieChart)
        var entries = [PieChartDataEntry]()
        for item in answersArray{
            let answersValue = Double(item.percetnage ?? 0)
            if answersValue > 0{
                entries.append(
                    PieChartDataEntry(
                        value: answersValue,
                        label: item.text
                    )
                )
            }
        }
        let set = PieChartDataSet(entries: entries, label: "")
        set.colors = ChartColorTemplates.colorful() + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
        let data = PieChartData(dataSet: set)
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = "%"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        pieChart.drawEntryLabelsEnabled = false
        pieChart.data = data
        
        self.contentView.backgroundColor = self.colorBackground
        self.viewBody.backgroundColor = self.colorBackground
    }
    
    
    
    
    
}
