//
//  NameCell.swift
//  test
//
//  Created by Joaquin Martinez on 22/12/21.
//  Copyright © 2021 Macbook. All rights reserved.
//

import Foundation
import UIKit

class NameCell : UITableViewCell, UITextFieldDelegate{
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var viewBody: UIView!
    
    public var colorBackground : UIColor = .white

    override func awakeFromNib() {
        super.awakeFromNib()
        self.textFieldName.delegate = self
    }
    
    override func layoutSubviews() {
        self.contentView.backgroundColor = self.colorBackground
        self.viewBody.backgroundColor = self.colorBackground
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
        let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
        let typedCharacterSet = CharacterSet(charactersIn: string)
        let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
        return alphabet
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
    
    
}
